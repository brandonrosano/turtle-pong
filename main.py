#Basic Pong Python 3

import turtle
import os

wn = turtle.Screen()
wn.title("Turtle Pong")
wn.bgcolor("black")
wn.setup(width=800, height=600)
wn.tracer(0)

#Score
score_a = 0
score_b = 0

#Paddle A
paddle_a = turtle.Turtle()
paddle_a.speed(0)
paddle_a.shape("square")
paddle_a.color("white")
paddle_a.shapesize(stretch_wid=5, stretch_len=1)
paddle_a.penup()
paddle_a.goto(-350, 0)

#Paddle B
paddle_b = turtle.Turtle()
paddle_b.speed(0)
paddle_b.shape("square")
paddle_b.color("white")
paddle_b.shapesize(stretch_wid=5, stretch_len=1)
paddle_b.penup()
paddle_b.goto(350, 0)

#Ball
ball = turtle.Turtle()
ball.speed(0)
ball.shape("square")
ball.color("white")
ball.penup()
ball.goto(0, 0)

#Ball Speed
ball.dx = .12
ball.dy = -.12

#Score Text
pen = turtle.Turtle()
pen.speed(0)
pen.color("white")
pen.penup()
pen.hideturtle()
pen.goto(0, 250)
pen.write("Player 1: 0  Player 2: 0", align="center", font=("Courier 10 Pitch", 24, "normal"))

#Paddle Functions
def paddle_a_up():
    y = paddle_a.ycor()
    y += 20
    paddle_a.sety(y)

def paddle_a_down():
    y = paddle_a.ycor()
    y -= 20
    paddle_a.sety(y)

def paddle_b_up():
    y = paddle_b.ycor()
    y += 20
    paddle_b.sety(y)

def paddle_b_down():
    y = paddle_b.ycor()
    y -= 20
    paddle_b.sety(y)

#Paddle Keyboard Bindings
wn.listen()
wn.onkeypress(paddle_a_up, "w")
wn.onkeypress(paddle_a_down, "s")
wn.onkeypress(paddle_b_up, "Up")
wn.onkeypress(paddle_b_down, "Down")

#Main Game Loop
while True:
    wn.update()

#Ball Movement
    ball.setx(ball.xcor() + ball.dx)
    ball.sety(ball.ycor() + ball.dy)

#Paddle Collision Checking With Borders
    #Left Paddle
    if paddle_a.ycor() >= 250:
        paddle_a.sety(250)
    elif paddle_a.ycor() <= -250:
        paddle_a.sety(-250)

    #Right Paddle
    if paddle_b.ycor() >= 250:
        paddle_b.sety(250)
    elif paddle_b.ycor() <= -250:
        paddle_b.sety(-250)

#Border Checking
    #Top-Border
    if ball.ycor() > 290:
        ball.sety(290)
        ball.dy *= -1
        os.system("aplay bounce.wav&")

    #Bottom-Border
    if ball.ycor() < -290:
        ball.sety(-290)
        ball.dy *= -1
        os.system("aplay bounce.wav&")

    #Left-Border
    if ball.xcor() < -395:
        ball.goto(0, 0)
        ball.dx *= -1
        score_b += 1
        pen.clear()
        pen.write("Player 1: {}  Player 2: {}".format(score_a, score_b), align="center", font=("Courier 10 Pitch", 24, "normal"))

    #Right-Border
    if ball.xcor() > 395:
        ball.goto(0, 0)
        ball.dx *= -1
        score_a += 1
        pen.clear()
        pen.write("Player 1: {}  Player 2: {}".format(score_a, score_b), align="center", font=("Courier 10 Pitch", 24, "normal"))

#Ball Collision Checking
    #Right Paddle
    if (ball.xcor() > 340 and ball.xcor() < 350) and (ball.ycor() < paddle_b.ycor() + 52 and ball.ycor() > paddle_b.ycor() - 52):
        ball.setx(340)
        ball.dx *= -1
        os.system("aplay bounce.wav&")

    #Left Paddle
    if (ball.xcor() < -340 and ball.xcor() > -350) and (ball.ycor() < paddle_a.ycor() + 52 and ball.ycor() > paddle_a.ycor() - 52):
        ball.setx(-340)
        ball.dx *= -1
        os.system("aplay bounce.wav&")

